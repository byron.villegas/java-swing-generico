package cl.villegas.repository;

import java.util.ArrayList;
import cl.villegas.configuration.Column;

/**
 * Interfaz generica para operaciones de base de datos
 *
 * @author Byron Villegas Moya
 * @version 1.0, 13/01/2022
 * @since 1.0
 */
public interface GenericRepository {
    void save(String table, ArrayList<Column> columns, ArrayList<String> values);
    ArrayList<Object[]> findAll(String table, ArrayList<Column> columns);
}