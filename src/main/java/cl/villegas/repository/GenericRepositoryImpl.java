package cl.villegas.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import cl.villegas.configuration.Column;
import cl.villegas.util.ConnectionUtil;
import cl.villegas.util.SqlUtil;

/**
 * Clase de implementacion generica para operaciones de base de datos
 *
 * @author Byron Villegas Moya
 * @version 1.0, 13/01/2022
 * @since 1.0
 */
public class GenericRepositoryImpl implements GenericRepository {
    private Connection connection = ConnectionUtil.getConnection();

    @Override
    public void save(String table, ArrayList<Column> columns, ArrayList<String> values) {
        try {
            String sql = SqlUtil.generateStatement("INSERT", table, columns, "(", ",", ")");
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            for (int i = 0; i < columns.size(); i++) {
                switch (columns.get(i).getType().toUpperCase()) {
                    case "STRING":
                        preparedStatement.setString(i + 1, String.valueOf(values.get(i)));
                        break;
                    case "BYTE":
                        preparedStatement.setByte(i + 1, Byte.parseByte(values.get(i)));
                        break;
                    case "DATE":
                        preparedStatement.setDate(i + 1, Date.valueOf(SqlUtil.parseDate(values.get(i))));
                        break;
                }
            }
            preparedStatement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public ArrayList<Object[]> findAll(String table, ArrayList<Column> columns) {
        ArrayList<Object[]> data = new ArrayList<>();
        try {
            String sql = SqlUtil.generateStatement("SELECT", table, columns, "", ",", "");
            Statement statement = connection.createStatement();
            statement.execute(sql);
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()) {
                Object[] row = new Object[columns.size()];
                for (int i = 0; i < columns.size(); i++) {
                    switch (columns.get(i).getType().toUpperCase()) {
                        case "STRING":
                            row[i] = resultSet.getString(columns.get(i).getName());
                            break;
                        case "BYTE":
                            row[i] = resultSet.getByte(columns.get(i).getName());
                            break;
                        case "LONG":
                            row[i] = resultSet.getLong(columns.get(i).getName());
                            break;
                        case "DATE":
                            row[i] = resultSet.getDate(columns.get(i).getName());
                            break;
                    }
                }
                data.add(row);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return data;
    }
}