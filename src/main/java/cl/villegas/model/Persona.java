package cl.villegas.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Clase persona entidad de la tabla PERSONA
 *
 * @author Byron Villegas
 * @version 1.0, 05/02/2022
 * @since 1.0
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Builder
public class Persona {
    private long id;
    private String rut;
    private String nombres;
    private String apellidos;
    private byte edad;
    private String fechaNacimiento;
}