package cl.villegas.util;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.*;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import org.yaml.snakeyaml.Yaml;
import cl.villegas.configuration.Menu;
import cl.villegas.configuration.MenuItem;
import cl.villegas.configuration.Panel;
import cl.villegas.constants.Constants;
import cl.villegas.repository.GenericRepositoryImpl;
import cl.villegas.configuration.Component;
import cl.villegas.configuration.FrameConfig;

/**
 * Clase utilitaria para generar distintos tipos de componentes
 * 
 * @author Byron Villegas
 * @version 1.0, 19/12/2021
 * @since 1.0
 */
public class FrameUtil {
    private static final Gson gson = new Gson();
    private static final Yaml yaml = new Yaml();

    /**
     * Metodo encargado de cambiar el look and feel a windows
     * 
     * @since 1.0
     */
    public static void setUpLookAndFeel() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException
                | UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Metodo encargado de generar el frame por defecto
     * 
     * @return configuracion de la interfaz
     * @since 1.0
     */
    private static FrameConfig setUpDefaultFrame() {
        Panel panel = new Panel("", 0, 0, new ArrayList<>());
        return new FrameConfig("POR DEFECTO", "", Constants.CloseOperationName.DISPOSE, 300, 300, null, false, true,
                null, panel, "", new ArrayList<>());
    }

    /**
     * Metodo encargado de obtener la configuracion de la interfaz mediante el archivo
     * 
     * @param file archivo de configuracion
     * @return configuracion de la interfaz
     * @since 1.0
     */
    public static FrameConfig getFrameConfigByFile(String file) {
        Properties properties = ConfigUtil.getPropertiesByFile(Constants.App.PROPERTIES_FILE);
        switch (FileUtil.getExtension(file).toUpperCase()) {
            case Constants.FileExtension.JSON:
                return gson.fromJson(
                        ConfigUtil.getJsonReaderByFile(properties.getProperty(Constants.PropertyName.FRAME_DIR) + file),
                        FrameConfig.class);
            case Constants.FileExtension.YAML:
                return yaml.loadAs(
                        ConfigUtil.getYamlReaderByFile(properties.getProperty(Constants.PropertyName.FRAME_DIR) + file),
                        FrameConfig.class);
            default:
                return setUpDefaultFrame();
        }
    }

    /**
     * Metodo encargado de obtener el color mediante el nombre
     *
     * @param name nombre del color definida en <code>Color</code>:
     *             <code>BLACK</code>,
     *             <code>RED</code>,
     *             <code>BLUE</code>,
     *             <code>GREEN</code>,
     *             <code>YELLOW</code>.
     * @return color
     * @since 1.0
     * @see Color
     */
    public static Color getColorByName(String name) {
        try {
            if (name == null || name.isEmpty())
                return Color.BLACK;
            if (name.toUpperCase().contains(Constants.ColorName.DARK))
                return (Color) Color.class
                        .getDeclaredField(
                                Constants.ColorName.DARK + "_" + name.toUpperCase().split(Constants.ColorName.DARK)[1])
                        .get(null);
            if (name.toUpperCase().contains(Constants.ColorName.LIGHT))
                return (Color) Color.class.getDeclaredField(
                        Constants.ColorName.LIGHT + "_" + name.toUpperCase().split(Constants.ColorName.LIGHT)[1])
                        .get(null);
            return (Color) Color.class.getDeclaredField(name.toUpperCase()).get(null);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            return Color.BLACK;
        }
    }

    /**
     * Metodo encargado de obtener la posicion horizontal mediante el nombre
     * 
     * @param name nombre de la posicion definida en <code>SwingConstants</code>:
     *             <code>LEFT</code>,
     *             <code>CENTER</code>,
     *             <code>RIGHT</code>,
     *             <code>LEADING</code>,
     *             <code>TRAILING</code>.
     * @return posicion
     * @since 1.0
     * @see SwingConstants
     */
    public static int getHorizontalAlignmentByName(String name) {
        try {
            if (name == null || name.isEmpty())
                return SwingConstants.LEFT;
            if (!name.toUpperCase().equals(Constants.AlignmentName.LEFT)
                    && !name.toUpperCase().equals(Constants.AlignmentName.CENTER)
                    && !name.toUpperCase().equals(Constants.AlignmentName.RIGHT)
                    && !name.toUpperCase().equals(Constants.AlignmentName.LEADING)
                    && !name.toUpperCase().equals(Constants.AlignmentName.TRAILING))
                return SwingConstants.LEFT;
            return (int) SwingConstants.class.getDeclaredField(name.toUpperCase()).get(null);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            return SwingConstants.LEFT;
        }
    }

    /**
     * Metodo encargado de obtener la posicion vertical mediante el nombre
     * 
     * @param name nombre de la posicion definida en <code>SwingConstants</code>:
     *             <code>TOP</code>,
     *             <code>CENTER</code>,
     *             <code>BOTTOM</code>.
     * @return posicion
     * @since 1.0
     * @see SwingConstants
     */
    public static int getVerticalAlignmentByName(String name) {
        try {
            if (name == null || name.isEmpty())
                return SwingConstants.CENTER;
            if (!name.toUpperCase().equals(Constants.AlignmentName.TOP)
                    && !name.toUpperCase().equals(Constants.AlignmentName.CENTER)
                    && !name.toUpperCase().equals(Constants.AlignmentName.BOTTOM))
                return SwingConstants.CENTER;
            return (int) SwingConstants.class.getDeclaredField(name.toUpperCase()).get(null);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            return SwingConstants.CENTER;
        }
    }

    /**
     * Metodo encargado de obtener el cierre por defecto mediante el nombre
     * 
     * @param name nombre del cierre valores:
     *             <code>NOTHING</code>,
     *             <code>HIDE</code>,
     *             <code>DISPOSE</code>,
     *             <code>EXIT</code>.
     * @return cierre por defecto
     * @since 1.0
     * @see WindowConstants
     */
    public static int getDefaultCloseOperationByName(String name) {
        try {
            if (name == null || name.isEmpty())
                return WindowConstants.DO_NOTHING_ON_CLOSE;
            if (!name.toUpperCase().equals(Constants.CloseOperationName.NOTHING)
                    && !name.toUpperCase().equals(Constants.CloseOperationName.HIDE)
                    && !name.toUpperCase().equals(Constants.CloseOperationName.DISPOSE)
                    && !name.toUpperCase().equals(Constants.CloseOperationName.EXIT))
                return WindowConstants.DO_NOTHING_ON_CLOSE;
            if (name.toUpperCase().contains(Constants.CloseOperationName.NOTHING))
                return (int) WindowConstants.class.getDeclaredField(Constants.CloseOperationName.DO_NOTHING_ON_CLOSE)
                        .get(null);
            return (int) WindowConstants.class.getDeclaredField(name.toUpperCase() + "_ON_CLOSE").get(null);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException ex) {
            return WindowConstants.DO_NOTHING_ON_CLOSE;
        }
    }

    /**
     * Metodo encargado de obtener el tipo de barcode mediante el nombre
     * @param name nombre
     * @return tipo de barcode
     * @since 1.0
     */
    public static BarcodeFormat getBarcodeTypeByName(String name) {
        try {
            if (name == null || name.isEmpty())
                return BarcodeFormat.QR_CODE;
            return (BarcodeFormat) BarcodeFormat.class.getDeclaredField(name.toUpperCase()).get(null);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            return BarcodeFormat.QR_CODE;
        }
    }

    /**
     * Metodo encargado de generar los componentes de la interfaz
     * 
     * @param frameConfig configuracion de la interfaz
     * @param frame interfaz
     * @since 1.0
     */
    public static void setUpComponents(FrameConfig frameConfig, JFrame frame) {
        JPanel panel = new JPanel();

        if (!frameConfig.getPanel().getLayoutManager().isEmpty()) {
            switch(frameConfig.getPanel().getLayoutManager()) {
                case "BoxLayout":
                    panel.setLayout(new BoxLayout(panel, 0));
                    break;
                case "GridLayout":
                    panel.setLayout(new GridLayout(frameConfig.getPanel().getRows(), frameConfig.getPanel().getColumns()));
                    break;
            }
        }
        for (Component component : frameConfig.getPanel().getComponents()) {
            switch (component.getType().toUpperCase()) {
                case Constants.ComponentTypeName.IMAGE:
                    JLabel image = new JLabel(new ImageIcon(BarcodeUtil.generateBarcodeImageFromType(getBarcodeTypeByName(component.getBarcode()), component.getText(), 110, 110)));
                    panel.add(image);
                    break;
                case Constants.ComponentTypeName.BUTTON:
                    JButton button = new JButton(component.getText());
                    button.setHorizontalAlignment(getHorizontalAlignmentByName(component.getHorizontalAlignment()));
                    button.setVerticalAlignment(getVerticalAlignmentByName(component.getVerticalAlignment()));
                    button.setEnabled(component.isEnabled());
                    button.setForeground(getColorByName(component.getColor()));
                    if (component.isBold()) {
                        Font font = button.getFont();
                        button.setFont(font.deriveFont(font.getStyle() | Font.BOLD));
                    }
                    button.setFocusable(component.isFocusable());
                    button.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent arg) {
                            switch (component.getAction().toUpperCase()) {
                                case Constants.ComponentAction.EXIT:
                                    frame.dispose();
                                    break;
                                case Constants.ComponentAction.SAVE:
                                    ArrayList<String> values = new ArrayList<>();
                                    for (java.awt.Component componentAux : panel.getComponents()) {
                                        if (componentAux.getClass().getName().toString()
                                                .equals("javax.swing.JTextField")) {
                                            values.add(((JTextField) componentAux).getText());
                                        }
                                    }
                                    GenericRepositoryImpl genericRepositoryImpl = new GenericRepositoryImpl();
                                    genericRepositoryImpl.save(frameConfig.getTable(), frameConfig.getColumns(),
                                            values);
                                    ArrayList<Object[]> data = genericRepositoryImpl.findAll(frameConfig.getTable(),
                                            frameConfig.getColumns());
                                    for (int i = 0; i < data.size(); i++) {
                                        String response = "";
                                        for (int j = 0; j < data.get(i).length; j++) {
                                            switch (frameConfig.getColumns().get(j).getType().toUpperCase()) {
                                                case Constants.ColumnType.BYTE:
                                                    response += " " + ((byte) data.get(i)[j]);
                                                    break;
                                                case Constants.ColumnType.SHORT:
                                                    response += " " + ((short) data.get(i)[j]);
                                                    break;
                                                case Constants.ColumnType.INT:
                                                    response += " " + ((int) data.get(i)[j]);
                                                    break;
                                                case Constants.ColumnType.LONG:
                                                    response += " " + ((long) data.get(i)[j]);
                                                    break;
                                                case Constants.ColumnType.FLOAT:
                                                    response += " " + ((float) data.get(i)[j]);
                                                    break;
                                                case Constants.ColumnType.DOUBLE:
                                                    response += " " + ((double) data.get(i)[j]);
                                                    break;
                                                case Constants.ColumnType.CHAR:
                                                    response += " " + ((char) data.get(i)[j]);
                                                    break;
                                                case Constants.ColumnType.BOOLEAN:
                                                    response += " " + ((boolean) data.get(i)[j]);
                                                    break;
                                                case Constants.ColumnType.STRING:
                                                    response += " " + String.valueOf(data.get(i)[j]);
                                                    break;
                                                case Constants.ColumnType.DATE:
                                                    response += " " + ((Date) data.get(i)[j]);
                                                    break;
                                            }
                                        }
                                        System.out.println(response);
                                    }
                                    break;
                            }
                        }
                    });
                    panel.add(button);
                    break;
                case Constants.ComponentTypeName.LABEL:
                    JLabel label = new JLabel(component.getText());
                    label.setHorizontalAlignment(getHorizontalAlignmentByName(component.getHorizontalAlignment()));
                    label.setVerticalAlignment(getVerticalAlignmentByName(component.getVerticalAlignment()));
                    label.setEnabled(component.isEnabled());
                    label.setForeground(getColorByName(component.getColor()));
                    if (component.isBold()) {
                        Font font = label.getFont();
                        label.setFont(font.deriveFont(font.getStyle() | Font.BOLD));
                    }
                    label.setFocusable(component.isFocusable());
                    panel.add(label);
                    break;
                case Constants.ComponentTypeName.TEXT_FIELD:
                    JTextField textField = new JTextField(component.getText());
                    textField.setName(component.getName());
                    textField.setHorizontalAlignment(getHorizontalAlignmentByName(component.getHorizontalAlignment()));
                    textField.setEnabled(component.isEnabled());
                    textField.setForeground(getColorByName(component.getColor()));
                    if (component.isBold()) {
                        Font font = textField.getFont();
                        textField.setFont(font.deriveFont(font.getStyle() | Font.BOLD));
                    }
                    textField.setFocusable(component.isFocusable());
                    textField.addKeyListener(new KeyListener() {
                        @Override
                        public void keyTyped(KeyEvent keyEvent) {
                            if (!component.getAllowedCharacters().contains(String.valueOf(keyEvent.getKeyChar()))) {
                                keyEvent.consume();
                            }
                            textField.setText(textField.getText().replaceAll(" +", " ")); // Elimina el doble espacio
                            if (textField.getText().length() > component.getMaxLength() - 1) {
                                keyEvent.consume();
                                frame.getToolkit().beep();
                            }
                        }

                        @Override
                        public void keyPressed(KeyEvent keyEvent) {

                        }

                        @Override
                        public void keyReleased(KeyEvent keyEvent) {
                            if (textField.getText().length() < component.getMinLength()
                                    || textField.getText().length() > component.getMaxLength()) {
                                textField.setToolTipText(component.getLengthMessage());
                                textField.setBorder(BorderFactory.createLineBorder(Color.RED));
                                return;
                            } else {
                                textField.setToolTipText(null);
                                textField.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
                            }

                            if (component.getMaxSize() != 0) {
                                int value = Integer.parseInt(textField.getText());
                                if (value < component.getMinSize() || value > component.getMaxSize()) {
                                    textField.setToolTipText(component.getSizeMessage());
                                    textField.setBorder(BorderFactory.createLineBorder(Color.RED));
                                    return;
                                } else {
                                    textField.setToolTipText(null);
                                    textField.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
                                }
                            }

                            if (component.isPatternEnabled()) {
                                Pattern pattern = Pattern.compile(component.getPatternExp());
                                Matcher matcher = pattern.matcher(textField.getText());
                                if (matcher.matches()) {
                                    textField.setToolTipText(null);
                                    textField.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));

                                    switch (component.getPatternValidation().toUpperCase()) {
                                        case Constants.PatternValidation.RUT:
                                            if (RutUtil.isValidFormatRut(textField.getText())
                                                    && RutUtil.isValidRut(textField.getText())) {
                                                textField.setToolTipText(null);
                                                textField.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
                                            } else {
                                                textField.setToolTipText(Constants.PatternValidation.RUT_ERROR_MESSAGE);
                                                textField.setBorder(BorderFactory.createLineBorder(Color.RED));
                                            }
                                            break;
                                        case Constants.PatternValidation.DATE:
                                            if (textField.getText().length() == component.getMaxLength()) {
                                                int year = Integer.parseInt(textField.getText().split("/")[2]);
                                                int month = Integer.parseInt(textField.getText().split("/")[1]);
                                                int day = Integer.parseInt(textField.getText().split("/")[0]);
                                                if (DateUtil.isDateValid(year, month, day)) {
                                                    textField.setToolTipText(null);
                                                    textField.setBorder(
                                                            BorderFactory.createLineBorder(Color.LIGHT_GRAY));
                                                } else {
                                                    textField.setToolTipText(Constants.PatternValidation.DATE_ERROR_MESSAGE);
                                                    textField.setBorder(BorderFactory.createLineBorder(Color.RED));
                                                }
                                            }
                                            break;
                                    }
                                } else {
                                    textField.setToolTipText(component.getPatternMessage());
                                    textField.setBorder(BorderFactory.createLineBorder(Color.RED));
                                }
                            }
                        }
                    });
                    panel.add(textField);
                    break;
                default:
                    break;
            }
        }
        frame.add(panel);
    }

    /**
     * Metodo encargado de generar el menu de barra de la interfaz mediante la configuracion
     * 
     * @param frameConfig configuracion de la interfaz
     * @param frame interfaz
     * @since 1.0
     */
    public static void setUpMenuBar(FrameConfig frameConfig, JFrame frame) {
        if (frameConfig.getMenuBar() != null) {
            JMenuBar menuBar = new JMenuBar();
            for (Menu menu : frameConfig.getMenuBar().getMenus()) {
                JMenu jMenu = new JMenu(menu.getText());
                for (MenuItem menuItem : menu.getMenuItems()) {
                    JMenuItem jMenuItem = new JMenuItem(menuItem.getText());
                    jMenuItem.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent event) {
                            switch (menuItem.getType().toUpperCase()) {
                                case Constants.MenuItemType.EXIT:
                                    System.exit(0); // Stop Program
                                    break;
                                case Constants.MenuItemType.OPEN:
                                    FrameConfig frameConfig = getFrameConfigByFile(menuItem.getFrameConfigFile());
                                    setUpFrame(frameConfig);
                                    break;
                                default:
                                    break;
                            }
                        }
                    });
                    jMenu.add(jMenuItem);
                }
                menuBar.add(jMenu);
            }
            frame.setJMenuBar(menuBar);
        }
    }

    /**
     * Metodo encargado de crear la interfaz y desplegarla en pantalla mediante la configuracion
     *
     * @param frameConfig configuracion de la interfaz
     * @since 1.0
     */
    public static void setUpFrame(FrameConfig frameConfig) {
        JFrame frame = new JFrame();
        ImageIcon icon = new ImageIcon(FrameUtil.class.getResource(frameConfig.getIconPath()));
        frame.setIconImage(icon.getImage());
        frame.setTitle(frameConfig.getTitle());
        frame.setDefaultCloseOperation(getDefaultCloseOperationByName(frameConfig.getDefaultCloseOperation()));
        frame.setSize(frameConfig.getWidth(), frameConfig.getHeight());
        if (frameConfig.getLocationRelativeTo() == null)
            frame.setLocationRelativeTo(null);
        frame.setResizable(frameConfig.isResizable());
        setUpMenuBar(frameConfig, frame);
        setUpComponents(frameConfig, frame);
        frame.setVisible(frameConfig.isVisible());
    }
}