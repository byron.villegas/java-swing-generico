package cl.villegas.util;

import cl.villegas.constants.Constants;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.aztec.AztecWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.datamatrix.DataMatrixWriter;
import com.google.zxing.oned.CodaBarWriter;
import com.google.zxing.oned.Code128Writer;
import com.google.zxing.oned.Code39Writer;
import com.google.zxing.oned.Code93Writer;
import com.google.zxing.oned.EAN13Writer;
import com.google.zxing.oned.EAN8Writer;
import com.google.zxing.oned.ITFWriter;
import com.google.zxing.oned.UPCAWriter;
import com.google.zxing.oned.UPCEANWriter;
import com.google.zxing.oned.UPCEWriter;
import com.google.zxing.pdf417.PDF417Writer;
import com.google.zxing.qrcode.QRCodeWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class BarcodeUtil {

    public static byte[] generateBarcodeImageFromType(BarcodeFormat barcodeFormat, String text, int width, int height) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            BitMatrix bitMatrix;
            switch (barcodeFormat) {
                case AZTEC:
                    AztecWriter aztecWriter = new AztecWriter();
                    bitMatrix = aztecWriter.encode(text, barcodeFormat, width, height);
                    break;
                case CODABAR:
                    CodaBarWriter codaBarWriter = new CodaBarWriter();
                    bitMatrix = codaBarWriter.encode(text, barcodeFormat, width, height);
                    break;
                case CODE_39:
                    Code39Writer code39Writer = new Code39Writer();
                    bitMatrix = code39Writer.encode(text, barcodeFormat, width, height);
                    break;
                case CODE_93:
                    Code93Writer code93Writer = new Code93Writer();
                    bitMatrix = code93Writer.encode(text, barcodeFormat, width, height);
                    break;
                case CODE_128:
                    Code128Writer code128Writer = new Code128Writer();
                    bitMatrix = code128Writer.encode(text, barcodeFormat, width, height);
                    break;
                case DATA_MATRIX:
                    DataMatrixWriter dataMatrixWriter = new DataMatrixWriter();
                    bitMatrix = dataMatrixWriter.encode(text, barcodeFormat, width, height);
                    break;
                case EAN_8:
                    EAN8Writer ean8Writer = new EAN8Writer();
                    bitMatrix = ean8Writer.encode(text, barcodeFormat, width, height);
                    break;
                case EAN_13:
                    EAN13Writer ean13Writer = new EAN13Writer();
                    bitMatrix = ean13Writer.encode(text, barcodeFormat, width, height);
                    break;
                case ITF:
                    ITFWriter itfWriter = new ITFWriter();
                    bitMatrix = itfWriter.encode(text, barcodeFormat, width, height);
                    break;
                case PDF_417:
                    PDF417Writer pdf417Writer = new PDF417Writer();
                    bitMatrix = pdf417Writer.encode(text, barcodeFormat, width, height);
                    break;
                case QR_CODE:
                    QRCodeWriter qrCodeWriter = new QRCodeWriter();
                    bitMatrix = qrCodeWriter.encode(text, barcodeFormat, width, height);
                    break;
                case UPC_A:
                    UPCAWriter upcaWriter = new UPCAWriter();
                    bitMatrix = upcaWriter.encode(text, barcodeFormat, width, height);
                    break;
                case UPC_E:
                    UPCEWriter upceWriter = new UPCEWriter();
                    bitMatrix = upceWriter.encode(text, barcodeFormat, width, height);
                    break;
                case UPC_EAN_EXTENSION:
                    UPCEANWriter upceanWriter = new UPCEWriter();
                    bitMatrix = upceanWriter.encode(text, barcodeFormat, width, height);
                    break;
                default:
                    qrCodeWriter = new QRCodeWriter();
                    bitMatrix = qrCodeWriter.encode(text, barcodeFormat, width, height);
                    break;
            }
            MatrixToImageWriter.writeToStream(bitMatrix, Constants.FileExtension.PNG, byteArrayOutputStream);
        } catch (WriterException | IOException e) {
            e.printStackTrace();
        }
        return byteArrayOutputStream.toByteArray();
    }
}