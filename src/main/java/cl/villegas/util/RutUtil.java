package cl.villegas.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Clase utilitaria para el formateo de rut
 * 
 * @author Byron Villegas
 * @version 1.0, 19/12/2021
 * @since 1.0
 */
public class RutUtil {
  /**
   * Metodo encargado de quitarle los puntos a un rut
   *
   * <p>
   * Ejemplo: 18.699.827-8
   * <p>
   * Formateado: 18699827-8
   * 
   * @param value rut con puntos
   * @return rut sin puntos
   * @since 1.0
   */
  public static String formatWithoutDots(String value) {
    return value.replace(".", "");
  }

  /**
   * Metodo encargado de quitarle los ceros de la izquierda a un rut
   *
   * <p>
   * Ejemplo: 0018699827-8
   * <p>
   * Formateado: 18699827-8
   * 
   * @param value rut con ceros a la izquierda
   * @return rut sin ceros a la izquierda
   * @since 1.0
   */
  public static String formatWithoutZeros(String value) {
    int position = 0;
    for (int i = 0; i < value.length(); i++) {
      if (value.charAt(i) != '0') {
        position = i;
        i = value.length();
      }
    }
    return value.substring(position, value.length());
  }

  /**
   * Metodo encargado de validar si el formato de un rut es valido
   *
   * @param value rut
   * @return valido o no
   * @since 1.0
   */
  public static boolean isValidFormatRut(String value) {
    Pattern pattern = Pattern.compile("\\d{1,2}\\.\\d{3}\\.\\d{3}\\-[0-9kK]{1}");
    Matcher matcher = pattern.matcher(value);
    return matcher.matches() ? true : false;
  }

  /**
   * Metodo encargado de validar si el rut es valido con el digito verificador
   *
   * @param value rut
   * @return valido o no
   * @since 1.0
   */
  public static boolean isValidRut(String value) {
    String rut = value.split("-")[0].replace(".", "");
    String digitoVerificador = value.split("-")[1];
    int modulo = 0, digito = 1, valor = Integer.parseInt(rut);
    for (; valor != 0; valor = (int) Math.floor(valor /= 10))
      digito = (digito + valor % 10 * (9 - modulo++ % 6)) % 11;
    return digitoVerificador.toUpperCase().equals((digito > 0) ? String.valueOf((digito - 1)) : "K");
  }
}