package cl.villegas.util;

import java.util.ArrayList;
import cl.villegas.configuration.Column;
import cl.villegas.constants.Constants;

/**
 * Clase utilitaria para sentencias sql
 *
 * @author Byron Villegas
 * @version 1.0, 19/12/2021
 * @since 1.0
 */
public class SqlUtil {
    /**
     * Metodo encargado de indicar si la columna de x tabla es auto incrementada
     *
     * @param table la tabla
     * @param column la columna
     * @return si es auto incrementada o no
     * @since 1.0
     */
    public static boolean isAutoIncrementColumn(String table, String column) {
        String schema = ConfigUtil.getSqlStatementsByFile(Constants.Database.SCHEMA_FILE);
        String splitColumn = schema.split(Constants.SqlStatement.TABLE+" " + table)[1].split(column)[1].split(";")[0];
        return splitColumn.contains(",") ? splitColumn.split(",")[0].contains(Constants.SqlStatement.AUTO_INCREMENT)
                : splitColumn.contains(Constants.SqlStatement.AUTO_INCREMENT);
    }

    /**
     * Metodo encargado de concatenar los nombres de columnas en formato sql
     * 
     * @param columns columnas
     * @param sentenceStartWith caracter de inicio
     * @param sentenceEndWith caracter de fin
     * @param separator separador de columnas
     * @return la sentencia
     * @since 1.0
     */
    public static String generateColumnsStatement(String type, String table, ArrayList<Column> columns,
            String sentenceStartWith, String separator,
            String sentenceEndWith) {
        if (columns.size() == 1 && type.toUpperCase().equals(Constants.SqlType.INSERT)
                && !isAutoIncrementColumn(table, columns.get(0).getName()))
            return sentenceStartWith + columns.get(0).getName() + sentenceEndWith;
        String columnsStatement = "";
        Column firstColumn = columns.stream().filter(column -> !isAutoIncrementColumn(table, column.getName()))
                .findFirst().get();
        if ((columns.indexOf(firstColumn) + 1) == columns.size())
            return sentenceStartWith + firstColumn.getName() + sentenceEndWith;
        for (int i = 0; i < columns.size(); i++) {
            switch (type.toUpperCase()) {
                case Constants.SqlType.SELECT:
                    if (i == 0)
                        columnsStatement = sentenceStartWith + columns.get(i).getName() + separator;
                    if (i != 0 && i < columns.size() - 1)
                        columnsStatement = columnsStatement + columns.get(i).getName() + separator;
                    if (i == columns.size() - 1)
                        columnsStatement = columnsStatement + columns.get(i).getName() + sentenceEndWith;
                    break;
                case Constants.SqlType.INSERT:
                    if (!isAutoIncrementColumn(table, columns.get(i).getName())) {
                        if (i == columns.indexOf(firstColumn))
                            columnsStatement = sentenceStartWith + columns.get(i).getName() + separator;
                        if (i != columns.indexOf(firstColumn) && i < columns.size() - 1)
                            columnsStatement = columnsStatement + columns.get(i).getName() + separator;
                        if (i == columns.size() - 1)
                            columnsStatement = columnsStatement + columns.get(i).getName() + sentenceEndWith;
                    }
                    break;
            }
        }
        return columnsStatement;
    }

    /**
     * Metodo encargado de concatenar la cantidad de columnas en formato sql
     * 
     * @param columns columnas
     * @param sentenceStartWith caracter de inicio
     * @param separator separador de columnas
     * @param sentenceEndWith caracter de fin
     * @return la sentencia
     * @since 1.0
     */
    public static String generateInsertStatement(String table, ArrayList<Column> columns, String sentenceStartWith, String separator, String sentenceEndWith) {
        if (columns.size() == 1 && !isAutoIncrementColumn(table, columns.get(0).getName()))
            return sentenceStartWith + "?" + sentenceEndWith;
        String insertStatements = "";
        Column firstColumn = columns.stream().filter(column -> !isAutoIncrementColumn(table, column.getName())).findFirst().get();
        if ((columns.indexOf(firstColumn) + 1) == columns.size())
            return sentenceStartWith + "?" + sentenceEndWith;
        for (int i = 0; i < columns.size(); i++) {
            if (!isAutoIncrementColumn(table, columns.get(i).getName())) {
                if (i == columns.indexOf(firstColumn))
                    insertStatements = sentenceStartWith + "?" + separator;
                if (i != columns.indexOf(firstColumn) && i < columns.size() - 1)
                    insertStatements = insertStatements + "?" + separator;
                if (i == columns.size() - 1)
                    insertStatements = insertStatements + "?" + sentenceEndWith;
            }
        }
        return insertStatements;
    }

    /**
     * Metodo encargado de generar la sentencia sql
     * 
     * @param type tipo ejemplo: select / insert
     * @param table nombre de la tabla
     * @param columns columnas
     * @param sentenceStartWith caracter de inicio
     * @param separator separador de columnas
     * @param sentenceEndWith caracter de fin
     * @return la sentencia
     * @since 1.0
     */
    public static String generateStatement(String type, String table, ArrayList<Column> columns, String sentenceStartWith, String separator, String sentenceEndWith) {
        switch (type.toUpperCase()) {
            case Constants.SqlType.SELECT:
                return Constants.SqlStatement.SELECT + " "
                        + generateColumnsStatement(type, table, columns, sentenceStartWith, separator, sentenceEndWith)
                        + " "
                        + Constants.SqlStatement.FROM + " " + table;
            case Constants.SqlType.INSERT:
                return Constants.SqlStatement.INSERT_INTO + " " + table
                        + generateColumnsStatement(type, table, columns, sentenceStartWith, separator, sentenceEndWith)
                        + " "
                        + Constants.SqlStatement.VALUES + " "
                        + generateInsertStatement(table, columns, "(", ",", ")");
            default:
                return "";
        }
    }

    /**
     * Metodo encargado de transformar la fecha en formato dd/MM/yyyy a yyyy-MM-dd
     * 
     * @param date fecha
     * @return fecha transformada
     * @since 1.0
     */
    public static String parseDate(String date) {
        String[] dateSplit = {};
        if (date.contains("-"))
            dateSplit = date.split("-");
        if (date.contains("/"))
            dateSplit = date.split("/");
        return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];
    }
}