package cl.villegas.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import cl.villegas.constants.Constants;

/**
 * Clase utilitaria para la conexion a base de datos y la configuracion inicial
 * 
 * @author Byron Villegas
 * @version 1.0, 19/12/2021
 * @since 1.0
 */
public class ConnectionUtil {
    private static Connection connection;
    private static final String URL = Constants.Database.URL + System.getProperty(Constants.PropertyName.USER_HOME) + Constants.Database.FOLDER + Constants.Database.FILE;

    /**
     * Metodo encargado de generar la conexion a la base de datos
     * 
     * @return la conexion a la base de datos
     * @since 1.0
     */
    public static Connection getConnection() {
        try {
            Properties properties = ConfigUtil.getPropertiesByFile(Constants.App.PROPERTIES_FILE);
            Class.forName(properties.getProperty(Constants.PropertyName.DATABASE_DRIVER));
            if (connection == null)
                connection = DriverManager.getConnection(URL, properties.getProperty(Constants.PropertyName.DATABASE_USERNAME), properties.getProperty(Constants.PropertyName.DATABASE_PASSWORD));
            return connection;
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Metodo encargado de generar las configuraciones iniciales de la base de datos
     * 
     * @since 1.0
     */
    public static void setUpDatabase() {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            statement.execute(ConfigUtil.getSqlStatementsByFile(Constants.Database.SCHEMA_FILE));
            statement.execute(ConfigUtil.getSqlStatementsByFile(Constants.Database.DATA_FILE));
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}