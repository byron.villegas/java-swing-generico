package cl.villegas.util;

import java.time.DateTimeException;
import java.time.LocalDate;

/**
 * Clase utilitaria para varias operaciones a fechas
 * 
 * @author Byron Villegas
 * @version 1.0, 10/02/2022
 * @since 1.0
 */
public class DateUtil {
    /**
     * Metodo encargado de validar que una fecha sea valida
     * 
     * @param year anio
     * @param month mes
     * @param day dia
     * @return valida o no
     * @since 1.0
     */
    public static boolean isDateValid(int year, int month, int day) {
        boolean isDateValid = true;
        try {
            LocalDate.of(year, month, day);
        } catch (DateTimeException ex) {
            isDateValid = false;
        }
        return isDateValid;
    }
}