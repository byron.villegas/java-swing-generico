package cl.villegas.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import com.google.gson.stream.JsonReader;
import cl.villegas.constants.Constants;

/**
 * Clase utilitaria para obtener diferentes tipos de configuraciones
 * 
 * @author Byron Villegas
 * @version 1.0, 19/12/2021
 * @since 1.0
 */
public class ConfigUtil {
    /**
     * Metodo encargado de obtener las propiedades de la aplicacion mediante el archivo
     * 
     * @param file archivo properties
     * @return propiedades de la aplicacion
     * @since 1.0
     */
    public static Properties getPropertiesByFile(String file) {
        Properties properties = new Properties();
        try {
            properties.load(ConnectionUtil.class.getClassLoader().getResourceAsStream(file));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return properties;
    }

    /**
     * Metodo encargado de obtener la lectura del yaml de configuracion mediante el archivo
     *
     * @param file archivo yaml
     * @return lectura del yaml
     * @since 1.0
     */
    public static InputStream getYamlReaderByFile(String file) {
        return ConfigUtil.class.getResourceAsStream(file.indexOf("/") < 0 ? "/" + file : file);
    }

    /**
     * Metodo encargado de obtener la lectura del json de configuracion mediante el archivo
     * 
     * @param file archivo json
     * @return lectura del json
     * @since 1.0
     */
    public static JsonReader getJsonReaderByFile(String file) {
        try {
            Properties properties = getPropertiesByFile(Constants.App.PROPERTIES_FILE);
            return new JsonReader(new InputStreamReader(ConfigUtil.class.getResourceAsStream(file.indexOf("/") < 0 ? "/" + file : file), properties.getProperty(Constants.PropertyName.GSON_CHARSET)));
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Metodo encargado de obtener las sentencias sql mediante el archivo
     * 
     * @param file archivo sql
     * @return sentencias sql
     * @since 1.0
     */
    public static String getSqlStatementsByFile(String file) {
        try {
            Properties properties = ConfigUtil.getPropertiesByFile(Constants.App.PROPERTIES_FILE);
            return new String(Files.readAllBytes(Paths.get(ConfigUtil.class.getResource(properties.getProperty(Constants.PropertyName.DATABASE_DIR) + file).toURI())));
        } catch (IOException | URISyntaxException ex) {
            ex.printStackTrace();
            return "";
        }
    }
}