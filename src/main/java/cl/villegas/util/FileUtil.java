package cl.villegas.util;

/**
 * Clase utilitaria para varias operaciones a archivos
 * 
 * @author Byron Villegas
 * @version 1.0, 10/02/2022
 * @since 1.0
 */
public class FileUtil {
    /**
     * Metodo encargado de obtener la extension de un archivo
     * 
     * @param file el nombre del archivo
     * @return la extension
     * @since 1.0
     */
    public static String getExtension(String file) {
        return file.substring(file.lastIndexOf(".") + 1, file.length());
    }
}