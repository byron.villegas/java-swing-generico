package cl.villegas.constants;

/**
 * Clase con las constantes globales de la aplicacion
 * 
 * @author Byron Villegas
 * @version 1.0, 05/02/2022
 * @since 1.0
 */
public class Constants {
    /**
     * Clase con las constantes de app
     * 
     * @author Byron Villegas
     * @version 1.0, 05/02/2022
     * @since 1.0
     */
    public final class App {
        /** Application Frame Config File */
        public static final String FRAME_CONFIG_FILE = "application-config.json";
        /** Application Properties */
        public static final String PROPERTIES_FILE = "application.properties";
    }

    /**
     * Clase con las constantes de alignment name
     * 
     * @author Byron Villegas
     * @version 1.0, 05/02/2022
     * @since 1.0
     */
    public final class AlignmentName {
        /** Bottom Alignment Name */
        public static final String BOTTOM = "BOTTOM";
        /** Center Alignment Name */
        public static final String CENTER = "CENTER";
        /** Leading Alignment Name */
        public static final String LEADING = "LEADING";
        /** Left Alignment Name */
        public static final String LEFT = "LEFT";
        /** Right Alignment Name */
        public static final String RIGHT = "RIGHT";
        /** Top Alignment Name */
        public static final String TOP = "TOP";
        /** Trailing Alignment Name */
        public static final String TRAILING = "TRAILING";
    }

    /**
     * Clase con las constantes de close operation name
     * 
     * @author Byron Villegas
     * @version 1.0, 05/02/2022
     * @since 1.0
     */
    public final class CloseOperationName {
        /** Dispose Close Operation Name */
        public static final String DISPOSE = "DISPOSE";
        /** Do Nothing On Close Close Operation Name */
        public static final String DO_NOTHING_ON_CLOSE = "DO_NOTHING_ON_CLOSE";
        /** Exit Close Operation Name */
        public static final String EXIT = "EXIT";
        /** Hide Close Operation Name */
        public static final String HIDE = "HIDE";
        /** Nothing Close Operation Name */
        public static final String NOTHING = "NOTHING";
    }

    /**
     * Clase con las constantes de column type
     * 
     * @author Byron Villegas
     * @version 1.0, 10/02/2022
     * @since 1.0
     */
    public final class ColumnType {
        /** Byte Column Type */
        public static final String BYTE = "BYTE";
        /** Short Column Type */
        public static final String SHORT = "SHORT";
        /** Int Column Type */
        public static final String INT = "INT";
        /** Long Column Type */
        public static final String LONG = "LONG";
        /** Float Column Type */
        public static final String FLOAT = "FLOAT";
        /** Double Column Type */
        public static final String DOUBLE = "DOUBLE";
        /** Char Column Type */
        public static final String CHAR = "CHAR";
        /** Boolean Column Type */
        public static final String BOOLEAN = "BOOLEAN";
        /** String Column Type */
        public static final String STRING = "STRING";
        /** Date Column Type */
        public static final String DATE = "DATE";
    }

    /**
     * Clase con las constantes de component action
     * 
     * @author Byron Villegas
     * @version 1.0, 10/02/2022
     * @since 1.0
     */
    public final class ComponentAction {
        /** Exit Component Action */
        public static final String EXIT = "EXIT";
        /** Save Component Action */
        public static final String SAVE = "SAVE";
    }

    /**
     * Clase con las constantes de component type name
     * 
     * @author Byron Villegas
     * @version 1.0, 05/02/2022
     * @since 1.0
     */
    public final class ComponentTypeName {
        /** Button Component Type Name */
        public static final String BUTTON = "BUTTON";
        /** Image Component Type Name */
        public static final String IMAGE = "IMAGE";
        /** Label Component Type Name */
        public static final String LABEL = "LABEL";
        /** TextField Component Type Name */
        public static final String TEXT_FIELD = "TEXTFIELD";
    }

    /**
     * Clase con las constantes de color name
     * 
     * @author Byron Villegas
     * @version 1.0, 05/02/2022
     * @since 1.0
     */
    public final class ColorName {
        /** Dark Color Name */
        public static final String DARK = "DARK";
        /** Light Color Name */
        public static final String LIGHT = "LIGHT";
    }

    /**
     * Clase con las constantes de database
     * 
     * @author Byron Villegas
     * @version 1.0, 05/02/2022
     * @since 1.0
     */
    public final class Database {
        /** Data File */
        public static final String DATA_FILE = "data.sql";
        /** Database File */
        public static final String FILE = "test";
        /** Database Folder */
        public static final String FOLDER = "/h2-database/";
        /** Schema File */
        public static final String SCHEMA_FILE = "schema.sql";
        /** Database URL */
        public static final String URL = "jdbc:h2:file:";
    }

    /**
     * Clase con las constantes de file
     * 
     * @author Byron Villegas
     * @version 1.0, 05/02/2022
     * @since 1.0
     */
    public final class FileExtension {
        /** Json Extension */
        public static final String JSON = "JSON";
        /** Yaml Extension */
        public static final String YAML = "YAML";
        /** Png Extension */
        public static final String PNG = "PNG";
    }

    /**
     * Clase con las constantes de menu item type
     * 
     * @author Byron Villegas
     * @version 1.0, 10/02/2022
     * @since 1.0
     */
    public final class MenuItemType {
        public static final String EXIT = "EXIT";
        public static final String OPEN = "OPEN";
    }

    /**
     * Clase con las constantes de pattern validation
     * 
     * @author Byron Villegas
     * @version 1.0, 10/02/2022
     * @since 1.0
     */
    public final class PatternValidation {
        /** Rut Pattern Validation */
        public static final String RUT = "RUT";
        /** Rut Error Message */
        public static final String RUT_ERROR_MESSAGE = "Rut Inválido";
        /** Date Pattern Validation */
        public static final String DATE = "DATE";
        /** Date Error Message */
        public static final String DATE_ERROR_MESSAGE = "Fecha Inválida";
    }

    /**
     * Clase con las constantes de property name
     * 
     * @author Byron Villegas
     * @version 1.0, 05/02/2022
     * @since 1.0
     */
    public final class PropertyName {
        /** Database Dir Property Name */
        public static final String DATABASE_DIR = "database.dir";
        /** Database Driver Property Name */
        public static final String DATABASE_DRIVER = "database.driver";
        /** Database Password Property Name */
        public static final String DATABASE_PASSWORD = "database.password";
        /** Database Username Property Name */
        public static final String DATABASE_USERNAME = "database.username";
        /** Frame Dir Property Name */
        public static final String FRAME_DIR = "frame.dir";
        /** Gson Charset Property Name */
        public static final String GSON_CHARSET = "gson.charset";
        /** User Home Property Name */
        public static final String USER_HOME = "user.home";
    }

    /**
     * Clase con las constantes de sql statement
     * 
     * @author Byron Villegas
     * @version 1.0, 10/02/2022
     * @since 1.0
     */
    public final class SqlStatement {
        public static final String AUTO_INCREMENT = "AUTO_INCREMENT";
        public static final String FROM = "FROM";
        public static final String INSERT_INTO = "INSERT INTO";
        public static final String SELECT = "SELECT";
        public static final String TABLE = "TABLE";
        public static final String VALUES = "VALUES";
    }

    /**
     * Clase con las constantes de sql type
     * 
     * @author Byron Villegas
     * @version 1.0, 10/02/2022
     * @since 1.0
     */
    public final class SqlType {
        public static final String INSERT = "INSERT";
        public static final String SELECT = "SELECT";
    }
}