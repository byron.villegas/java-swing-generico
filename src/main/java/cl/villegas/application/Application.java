package cl.villegas.application;

import cl.villegas.configuration.FrameConfig;
import cl.villegas.constants.Constants;
import cl.villegas.util.ConnectionUtil;
import cl.villegas.util.FrameUtil;

/**
 * Clase principal de ejecucion de la aplicacion
 * 
 * @author Byron Villegas
 * @version 1.0, 19/12/2021
 * @since 1.0
 */
public class Application {
    /**
     * Metodo principal para la ejecucion de la aplicacion
     *
     * @param args Argumentos por consola
     * @since 1.0
     */
    public static void main(String[] args) {
        FrameUtil.setUpLookAndFeel();
        FrameConfig frameConfig = FrameUtil.getFrameConfigByFile(Constants.App.FRAME_CONFIG_FILE);
        FrameUtil.setUpFrame(frameConfig);
        ConnectionUtil.setUpDatabase();
    }
}