package cl.villegas.configuration;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Clase que contiene las configuraciones de los componentes
 * 
 * @author Byron Villegas
 * @version 1.0, 19/12/2021
 * @since 1.0
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Component {
    private String type;
    private String text;
    private String name;
    private String barcode;
    private String color;
    private boolean bold;
    private boolean enabled;
    private String horizontalAlignment;
    private String verticalAlignment;
    private boolean focusable;
    private String action;
    private boolean patternEnabled;
    private String patternExp;
    private String patternMessage;
    private String patternValidation;
    private String allowedCharacters;
    private int minLength;
    private int maxLength;
    private String lengthMessage;
    private int minSize;
    private int maxSize;
    private String sizeMessage;
}