package cl.villegas.configuration;

import java.util.ArrayList;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Clase que contiene la configuracion de la barra de menu
 * 
 * @author Byron Villegas
 * @version 1.0, 19/12/2021
 * @since 1.0
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class MenuBar {
    private ArrayList<Menu> menus;
}