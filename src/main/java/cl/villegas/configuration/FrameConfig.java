package cl.villegas.configuration;

import java.util.ArrayList;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Clase que contiene las configuraciones de las interfaces
 * 
 * @author Byron Villegas
 * @version 1.0, 19/12/2021
 * @since 1.0
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class FrameConfig {
    private String title;
    private String iconPath;
    private String defaultCloseOperation;
    private int width;
    private int height;
    private Object locationRelativeTo;
    private boolean resizable;
    private boolean visible;
    private MenuBar menuBar;
    private Panel panel;
    private String table;
    private ArrayList<Column> columns;
}