package cl.villegas.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Clase para los tests de ConnectionUtil
 * 
 * @author Byron Villegas
 * @version 1.0, 19/12/2021
 * @since 1.0
 * @see ConnectionUtil
 */
public class ConnectionUtilTest {

    /**
     * Metodo encargado de inicializar datos para cada test
     * 
     * @since 1.0
     */
    @Before
    public void setUp() {

    }

    /**
     * Metodo para test de validacion de conexion a base de datos
     * 
     * @since 1.0
     */
    @Test
    public void validateConnection() {
        assertNotNull(ConnectionUtil.getConnection());
    }

    /**
     * Metodo para test de validacion de configuracion inicial de la base de datos
     * 
     * @since 1.0
     */
    @Test
    public void validateSetUpDatabase() {
        ConnectionUtil.setUpDatabase();
        Connection connection = ConnectionUtil.getConnection();
        try {
            Statement statement = connection.createStatement();
            statement.execute("SELECT * FROM PERSONA");
            ResultSet resultSet = statement.getResultSet();
            if (resultSet.next())
                assertEquals("18.699.827-8", resultSet.getString("RUT"));
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Metodo encargado de limpiar los datos para cada test
     * 
     * @since 1.0
     */
    @After
    public void tearDown() {

    }
}