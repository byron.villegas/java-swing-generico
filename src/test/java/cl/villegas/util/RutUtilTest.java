package cl.villegas.util;

import static org.junit.Assert.assertEquals;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Clase para los tests de RutUtil
 * 
 * @author Byron Villegas
 * @version 1.0, 19/12/2021
 * @since 1.0
 * @see RutUtil
 */
public class RutUtilTest {
    private String rut;

    /**
     * Metodo encargado de inicializar datos para cada test
     * 
     * @since 1.0
     */
    @Before
    public void setUp() {
        this.rut = "011.111.111-1";
    }

    /**
     * Metodo para test de validacion de rut sin puntos
     * 
     * @since 1.0
     */
    @Test
    public void validateRutWithoutDots() {
        assertEquals("11111111-1", RutUtil.formatWithoutZeros(RutUtil.formatWithoutDots(rut)));
    }

    /**
     * Metodo para test de validacion de rut sin ceros a la izquierda
     * 
     * @since 1.0
     */
    @Test
    public void validateRutWithoutZeros() {
        assertEquals("11111111-1", RutUtil.formatWithoutZeros(RutUtil.formatWithoutDots(rut)));
    }

    /**
     * Metodo encargado de limpiar los datos para cada test
     * 
     * @since 1.0
     */
    @After
    public void tearDown() {
        this.rut = null;
    }
}