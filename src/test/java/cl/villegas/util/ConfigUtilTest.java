package cl.villegas.util;

import static org.junit.Assert.assertEquals;
import java.io.IOException;
import java.util.Properties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Clase para los tests de ConfigUtil
 * 
 * @author Byron Villegas
 * @version 1.0, 19/12/2021
 * @since 1.0
 * @see ConfigUtil
 */
public class ConfigUtilTest {
    private String jsonPath;
    private String sqlPath;

    /**
     * Metodo encargado de inicializar datos para cada test
     * 
     * @since 1.0
     */
    @Before
    public void setUp() {
        this.jsonPath = "test.json";
        this.sqlPath = "test.sql";
    }

    /**
     * Metodo para test de validacion de lectura de archivo json
     * 
     * @since 1.0
     */
    @Test
    public void validateJsonReader() {
        try {
            assertEquals(ConfigUtil.getJsonReaderByFile(jsonPath).hasNext(), true);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Metodo para test de validacion de lectura de archivo sql
     * 
     * @since 1.0
     */
    @Test
    public void validateSqlStatements() {
        assertEquals("SELECT 1;", ConfigUtil.getSqlStatementsByFile(sqlPath));
    }

    /**
     * Metodo para test de validacion de lectura de archivo properties
     * 
     * @since 1.0
     */
    @Test
    public void validateProperties() {
        Properties properties = ConfigUtil.getPropertiesByFile("application.properties");
        assertEquals("org.h2.Driver", properties.getProperty("database.driver"));
    }

    /**
     * Metodo encargado de limpiar los datos para cada test
     * 
     * @since 1.0
     */
    @After
    public void tearDown() {
        this.jsonPath = null;
        this.sqlPath = null;
    }
}