package cl.villegas.util;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Clase para los tests de FileUtil
 * 
 * @author Byron Villegas
 * @version 1.0, 19/12/2021
 * @since 1.0
 * @see FileUtil
 */
public class FileExtensionUtilTest {
    private String file;
    
    /**
     * Metodo encargado de inicializar datos para cada test
     * 
     * @since 1.0
     */
    @Before
    public void setUp() {
        this.file = "ejemplo.json.json";
    }

    /**
     * Metodo para test de validacion de extension de archivo
     * 
     * @since 1.0
     */
    @Test
    public void validateFileExtension() {
        assertEquals("json", FileUtil.getExtension(file));
    }

    /**
     * Metodo encargado de limpiar los datos para cada test
     * 
     * @since 1.0
     */
    @After
    public void tearDown() {
        this.file = null;
    }
}