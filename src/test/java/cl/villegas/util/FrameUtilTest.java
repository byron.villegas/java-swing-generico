package cl.villegas.util;

import static org.junit.Assert.assertEquals;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JLabel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import cl.villegas.configuration.FrameConfig;

/**
 * Clase para los tests de FrameUtil
 * 
 * @author Byron Villegas Moya
 * @version 1.0, 21/12/2021
 * @since 1.0
 * @see FrameUtil
 */
public class FrameUtilTest {
    private String frameConfigPathJson;
    private String frameConfigPathYaml;
    private String color;
    private String horizontalAlignment;
    private String verticalAlignment;
    private String defaultCloseOperation;

    /**
     * Metodo encargado de inicializar datos para cada test
     * 
     * @since 1.0
     */
    @Before
    public void setUp() {
        this.frameConfigPathJson = "test-config.json";
        this.frameConfigPathYaml = "test-config.yaml";
        this.color = "darkgray";
        this.horizontalAlignment = "center";
        this.verticalAlignment = "top";
        this.defaultCloseOperation = "hide";
    }

    /**
     * Metodo para test de validacion de configuracion de interfaz mediante archivo de configuracion json
     * 
     * @since 1.0
     */
    @Test
    public void validateFrameConfigByJsonFile() {
        FrameConfig frameConfig = FrameUtil.getFrameConfigByFile(frameConfigPathJson);
        assertEquals("Test Json", frameConfig.getTitle());
    }

    /**
     * Metodo para test de validacion de configuracion de interfaz mediante archivo de configuracion yaml
     * 
     * @since 1.0
     */
    @Test
    public void validateFrameConfigByYamlFile() {
        FrameConfig frameConfig = FrameUtil.getFrameConfigByFile(frameConfigPathYaml);
        assertEquals("Test Yaml", frameConfig.getTitle());
    }

    /**
     * Metodo para test de validacion de la obtencion del color
     * 
     * @since 1.0
     */
    @Test
    public void validateColor() {
        assertEquals(Color.DARK_GRAY, FrameUtil.getColorByName(color));
    }

    /**
     * Metodo para test de validacion de la obtencion de la posicion horizontal
     * 
     * @since 1.0
     */
    @Test
    public void validateHorizontalAlignment() {
        assertEquals(JLabel.CENTER, FrameUtil.getHorizontalAlignmentByName(horizontalAlignment));
    }

    /**
     * Metodo para test de validacion de la obtencion de la posicion vertical
     * 
     * @since 1.0
     */
    @Test
    public void validateVerticalAlignment() {
        assertEquals(JLabel.TOP, FrameUtil.getVerticalAlignmentByName(verticalAlignment));
    }

    /**
     * Metodo para test de validacion de la obtencion del cierre por defecto
     * 
     * @since 1.0
     */
    @Test
    public void validateDefaultCloseOperation() {
        assertEquals(JFrame.HIDE_ON_CLOSE, FrameUtil.getDefaultCloseOperationByName(defaultCloseOperation));
    }

    /**
     * Metodo encargado de limpiar los datos para cada test
     * 
     * @since 1.0
     */
    @After
    public void tearDown() {
        this.frameConfigPathJson = null;
        this.frameConfigPathYaml = null;
        this.color = null;
        this.horizontalAlignment = null;
        this.verticalAlignment = null;
        this.defaultCloseOperation = null;
    }
}