package cl.villegas.util;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import cl.villegas.configuration.Column;

/**
 * Clase para los tests de ConfigUtil
 * 
 * @author Byron Villegas
 * @version 1.0, 19/12/2021
 * @since 1.0
 * @see SqlUtil
 */
public class SqlUtilTest {
    private String table;
    private ArrayList<Column> columns;

    /**
     * Metodo encargado de inicializar datos para cada test
     * 
     * @since 1.0
     */
    @Before
    public void setUp() {
        this.table = "PERSONA";
        this.columns = new ArrayList<>();
        this.columns.add(new Column("ID", "long"));
        this.columns.add(new Column("RUT", "string"));
        this.columns.add(new Column("NOMBRES", "string"));
        this.columns.add(new Column("APELLIDOS", "string"));
        this.columns.add(new Column("EDAD", "byte"));
        this.columns.add(new Column("FECHANACIMIENTO", "date"));
    }

    /**
     * Metodo para test de validacion de generacion de sentencia de select
     * 
     * @since 1.0
     */
    @Test
    public void validateSelectStatement() {
        assertEquals("SELECT ID,RUT,NOMBRES,APELLIDOS,EDAD,FECHANACIMIENTO FROM PERSONA",
                SqlUtil.generateStatement("SELECT", this.table, columns, "", ",", ""));
    }

    /**
     * Metodo para test de validacion de generacion de sentencia de columnas
     * 
     * @since 1.0
     */
    @Test
    public void validateSqlColumnsStatements() {
        assertEquals("(RUT,NOMBRES,APELLIDOS,EDAD,FECHANACIMIENTO)",
                SqlUtil.generateColumnsStatement("INSERT", this.table, columns, "(", ",", ")"));
    }

    /**
     * Metodo para test de validacion de generacion de sentencia de insert
     * 
     * @since 1.0
     */
    @Test
    public void validateInsertStatement() {
        assertEquals("INSERT INTO PERSONA(RUT,NOMBRES,APELLIDOS,EDAD,FECHANACIMIENTO) VALUES (?,?,?,?,?)",
                SqlUtil.generateStatement("INSERT", this.table, columns, "(", ",", ")"));
    }

    @Test
    public void validateIsAutoIncrementColumn() {
        assertEquals(true, SqlUtil.isAutoIncrementColumn(table, "ID"));
    }
}